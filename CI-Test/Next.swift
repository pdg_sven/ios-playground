//
//  Nextz.swift
//  CI-Test
//
//  Created by Sven Lutz on 08.06.17.
//  Copyright © 2017 Porsche Digital GmbH. All rights reserved.
//

import UIKit

import MobileCenter
import MobileCenterAnalytics
import MobileCenterCrashes



class NextViewController: UIViewController {
    
    @IBOutlet weak var webView2: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView2.loadRequest(URLRequest(url: URL(string: "http://www.google.de")!));
        MSAnalytics.trackEvent("Porsche Web Called", withProperties: ["Website" : "http://www.google.de", "Params" : "Test2"])
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

