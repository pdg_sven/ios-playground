//
//  SecondViewController.swift
//  CI-Test
//
//  Created by Sven Lutz on 07.06.17.
//  Copyright © 2017 Porsche Digital GmbH. All rights reserved.
//

import UIKit

import MobileCenter
import MobileCenterAnalytics
import MobileCenterCrashes



class SecondViewController: UIViewController {
    
    @IBOutlet weak var webView: UIWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        webView.loadRequest(URLRequest(url: URL(string: "http://www.porsche.com/specials/en/porsche-digital/")!));
        MSAnalytics.trackEvent("Porsche Web Called", withProperties: ["Website" : "http://www.porsche.com/specials/en/porsche-digital/", "Params" : "Test"])
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

